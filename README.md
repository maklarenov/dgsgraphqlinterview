##Dockerize
docker
```
docker build --tag=dgs:latest
docker run -p9009:9090 dgs:latest
```
docker-compose
```
docker-compose config
docker-compose up --build
```

##Query
![newQuery.png](newQuery.png)


##Mutation
![newMutation.png](newMutation.png)

