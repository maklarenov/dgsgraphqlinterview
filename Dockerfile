FROM openjdk:11
MAINTAINER kz.zhaga
COPY target/dgsGraphqlInterview-0.0.1-SNAPSHOT.jar dgs-0.0.1.jar
ENTRYPOINT ["java","-jar","/dgs-0.0.1.jar"]