package kz.zhaga.dgsGraphqlInterview.dataFetcher;

import com.netflix.graphql.dgs.DgsQueryExecutor;
import graphql.ExecutionResult;
import kz.zhaga.dgsGraphqlInterview.model.Author;
import kz.zhaga.dgsGraphqlInterview.model.News;
import kz.zhaga.dgsGraphqlInterview.repository.NewsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NewsFetcherTest {


    @Autowired
    DgsQueryExecutor dgsQueryExecutor;

    @MockBean
    NewsRepository newsRepository;


    @BeforeEach
    public void before() {
        List<Author> authorList = List.of(
                new Author(1L, "SomeOne", "LastOne"),
                new Author(2L, "SecondOne", "SecondLastOne"));

        Mockito.when(newsRepository.findAll())
                .thenAnswer(invocation -> Collections.singletonList(News.builder().id(1L).status("Active").title("title").body("body")
                        .authors(authorList).build()));

        Mockito.when(newsRepository.findById(99L))
                .thenAnswer(invocation -> Optional.of(News.builder().id(99L).status("Active").title("title").body("body")
                        .authors(authorList).build()));

    }

    @Test
    void news() {
        List<String> firstName = dgsQueryExecutor.executeAndExtractJsonPath(
                " { news { id status title body authors { id firstName lastName}}}",
                "data.news[*].status");

        assertThat(firstName).contains("Active");
    }

    @Test
    void newsWithException() {
        Mockito.when(newsRepository.findAll()).thenThrow(new RuntimeException("nothing to see here"));
        ExecutionResult result = dgsQueryExecutor.execute(" { news { id status title body authors { id firstName lastName}}}");
        assertThat(result.getErrors()).isNotEmpty();
        assertThat(result.getErrors().get(0).getMessage()).isEqualTo("java.lang.RuntimeException: nothing to see here");
    }

    @Test
    void oneNew() {
        String body = dgsQueryExecutor.executeAndExtractJsonPath(
                " { new(id: 99) { id status title body authors { id firstName lastName}}}",
                "data.new.body");

        assertThat(body).contains("body");
    }

    @Test
    void newWithException() {
        Mockito.when(newsRepository.findById(99L)).thenThrow(new RuntimeException("nothing to see here"));
        ExecutionResult result = dgsQueryExecutor.execute(" { new(id: 99) { id status title body authors { id firstName lastName}}}");
        assertThat(result.getErrors()).isNotEmpty();
        assertThat(result.getErrors().get(0).getMessage()).isEqualTo("java.lang.RuntimeException: nothing to see here");
    }
}