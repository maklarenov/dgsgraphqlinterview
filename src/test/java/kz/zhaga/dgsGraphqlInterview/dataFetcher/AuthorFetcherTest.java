package kz.zhaga.dgsGraphqlInterview.dataFetcher;

import com.netflix.graphql.dgs.DgsQueryExecutor;
import graphql.ExecutionResult;
import kz.zhaga.dgsGraphqlInterview.model.Author;
import kz.zhaga.dgsGraphqlInterview.repository.AuthorRepository;
import org.assertj.core.util.Maps;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.graphql.GraphQlTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthorFetcherTest {
//    final MonoGraphQLClient monoGraphQLClient;


    @Autowired
    DgsQueryExecutor dgsQueryExecutor;

    @MockBean
    AuthorRepository authorRepository;


    @BeforeEach
    public void before() {

        Mockito.when(authorRepository.findAll())
                .thenAnswer(invocation -> Collections.singletonList(Author.builder().id(1L).firstName("mock firstName").lastName("lastName").build()));

        Mockito.when(authorRepository.findById(99L))
                .thenAnswer(invocation -> Optional.of(Author.builder().id(99L).firstName("mock firstName").lastName("lastName").build()));

        Author author = Author.builder().firstName("Test").lastName("Tested").build();
        Mockito.when(authorRepository.save(author)).thenAnswer(invocation -> author);


//        List<Author> authorList = List.of(
//                new Author(1L, "SomeOne", "LastOne"),
//                new Author(2L, "SecondOne", "SecondLastOne"));
//        Mockito.when(authorRepository.findAll())
//                .thenAnswer(invocation -> authorList.stream()
//                        .map(authList -> {
//                            Author author = Author.builder()
//                                    .id(authList.getId())
//                                    .firstName(authList.getFirstName())
//                                    .lastName(authList.getLastName())
//                                    .build();
//                            return author;
//                        }).collect(Collectors.toList()));
    }

    @Test
    void authors() {
        List<String> firstName = dgsQueryExecutor.executeAndExtractJsonPath(
                " { authors { id firstName lastName }}",
                "data.authors[*].firstName");

        assertThat(firstName).contains("mock firstName");
    }

    @Test
    void authorsWithException() {
        Mockito.when(authorRepository.findAll()).thenThrow(new RuntimeException("nothing to see here"));
        ExecutionResult result = dgsQueryExecutor.execute(" { authors { id firstName lastName }}");
        assertThat(result.getErrors()).isNotEmpty();
        assertThat(result.getErrors().get(0).getMessage()).isEqualTo("java.lang.RuntimeException: nothing to see here");
    }

    @Test
    void author() {
        String firstName = dgsQueryExecutor.executeAndExtractJsonPath(
                " { author(id: 99) { id firstName lastName }}",
                "data.author.firstName");

        assertThat(firstName).contains("mock firstName");
    }

    @Test
    void authorWithException() {
        Mockito.when(authorRepository.findById(99L)).thenThrow(new RuntimeException("nothing to see here"));
        ExecutionResult result = dgsQueryExecutor.execute(" { author(id: 99) { id firstName lastName }}");
        assertThat(result.getErrors()).isNotEmpty();
        assertThat(result.getErrors().get(0).getMessage()).isEqualTo("java.lang.RuntimeException: nothing to see here");
    }
//
//    @Test
//    void authorMutation() {
//        Author author = dgsQueryExecutor.executeAndExtractJsonPath(
//                " { author(authorInput: {firstName: \"Test\", lastName: \"Tested\"}) {id firstName lastName}}",
//                "data.author");
//
//        assertThat(author.getFirstName()).isEqualTo("Test");
//    }

//    @Test
//    void addReviewMutation() {
//        GraphQLQuery graphQLQueryRequest = new GraphQLQueryRequest(
//                AddReviewGraphQLQuery.newRequest()
//                        .review(SubmittedReview.newBuilder()
//                                .showId(1)
//                                .username("testuser")
//                                .starScore(5).build())
//                        .build(),
//                new AddReviewProjectionRoot().username().starScore());
//
//        ExecutionResult executionResult = dgsQueryExecutor.execute(graphQLQueryRequest.serialize());
//        assertThat(executionResult.getErrors()).isEmpty();
//
//        verify(reviewsService).reviewsForShow(1);
//    }
}