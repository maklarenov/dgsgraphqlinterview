package kz.zhaga.dgsGraphqlInterview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DgsGraphqlInterviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(DgsGraphqlInterviewApplication.class, args);
	}

}
