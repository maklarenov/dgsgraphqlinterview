package kz.zhaga.dgsGraphqlInterview.repository;

import kz.zhaga.dgsGraphqlInterview.model.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
}
