package kz.zhaga.dgsGraphqlInterview.dataFetcher;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingFieldSelectionSet;
import kz.zhaga.dgsGraphqlInterview.model.Author;
import kz.zhaga.dgsGraphqlInterview.model.AuthorInput;
import kz.zhaga.dgsGraphqlInterview.model.News;
import kz.zhaga.dgsGraphqlInterview.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@DgsComponent
public class AuthorFetcher {

    @Autowired
    AuthorRepository authorRepository;

    @DgsData(parentType = "Query", field = "authors")
    public List<Author> authors() {
        return authorRepository.findAll();
    }

    @DgsData(parentType = "Query", field = "author")
    public Author findById(@InputArgument("id") Long id) {
        Author author = authorRepository.findById(id).orElse(null);
        return author;
    }

    @DgsMutation
    public Author author(AuthorInput authorInput) {
        Author author = Author.builder()
                .firstName(authorInput.getFirstName())
                .lastName(authorInput.getLastName())
                .build();
        Author authorRes = authorRepository.save(author);
        return authorRes;
    }


}
