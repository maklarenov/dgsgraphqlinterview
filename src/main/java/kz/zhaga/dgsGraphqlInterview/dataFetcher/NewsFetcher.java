package kz.zhaga.dgsGraphqlInterview.dataFetcher;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.InputArgument;
import kz.zhaga.dgsGraphqlInterview.model.Author;
import kz.zhaga.dgsGraphqlInterview.model.AuthorInput;
import kz.zhaga.dgsGraphqlInterview.model.News;
import kz.zhaga.dgsGraphqlInterview.model.NewsInput;
import kz.zhaga.dgsGraphqlInterview.repository.AuthorRepository;
import kz.zhaga.dgsGraphqlInterview.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@DgsComponent
public class NewsFetcher {

    @Autowired
    NewsRepository newsRepository;

    @Autowired
    AuthorRepository authorRepository;


    @DgsData(parentType = "Query", field = "news")
    public List<News> news() {
        return newsRepository.findAll();
    }

    @DgsData(parentType = "Query", field = "new")
    public News findById(@InputArgument("id") Long id) {
        News news = newsRepository.findById(id).orElse(null);
        return news;
    }

    @DgsMutation
    public News news(NewsInput newsInput) {
        News news = News.builder()
                .status(newsInput.getStatus())
                .title(newsInput.getTitle())
                .body(newsInput.getBody())
                .authors(mapNewsAuthors(newsInput.getAuthors()))
                .build();
        News newsRes = newsRepository.save(news);
        return newsRes;
    }

    private List<Author> mapNewsAuthors(List<AuthorInput> authorInputs) {
        List<Author> authorList = authorInputs.stream().map(authInput -> {
            Author author = Author.builder()
                    .firstName(authInput.getFirstName())
                    .lastName(authInput.getLastName())
                    .build();
            return author;
        }).collect(Collectors.toList());
        return authorList;
    }
}
