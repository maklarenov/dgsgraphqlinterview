package kz.zhaga.dgsGraphqlInterview.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewsInput {

    private String status;
    private String title;
    private String body;
    private List<AuthorInput> authors;

}
