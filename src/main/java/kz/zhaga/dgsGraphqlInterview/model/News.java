package kz.zhaga.dgsGraphqlInterview.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "news")
public class News {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String status;
    private String title;
    private String body;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Author> authors;

}
